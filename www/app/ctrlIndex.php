<?php

class ctrlIndex extends ctrl 
{

    
    function index() 
    {    
        $this->posts = $this->db->query("SELECT * FROM post ORDER BY id DESC")->all();
        
        $this->out('posts.tpl.php');
    }
    
    public function add() 
    {
        if ($_POST)
        {
            $title = trim(htmlspecialchars($_POST['title']));
            $content = $_POST['content'];
            $email = $this->validEmail($_POST['email']);
            $user_id = $this->getUserId($_POST['email']);


            if($title && $content && $email){
                
                $id = $this->db->query("INSERT INTO post (title, content, user_id) VALUES (?,?,?)", $title, $content, $user_id)->getId();

                if (!empty($id)) 
                
                return header("Location: /?post/$id");
            } else {
                $this->title = $title;
                $this->content = $content;
                $this->email = $email;
                return $this->error = "Please fill all fields";
            }
        }

        $this->out('add.tpl.php');

    }

    public function edit($id) 
    {
        $id = (int)$id;
        if(!empty($_POST)) {

            $this->db->query("UPDATE post SET title = ?, content = ?", $_POST['title'], $_POST['content']);

        }
        
        $this->getPost($id);


        $this->title = $this->post['title'];
        $this->content = $this->post['content'];
        
        $user_id = (int)$this->post['user_id'];
        $this->getUser($user_id);

        $this->email = $this->user['email'];

        $this->out('add.tpl.php');
    }

    public function delete($id)
    {
        $this->db->query("DELETE FROM post WHERE id=?",$id);

        $this->redirect();
    }

    public function post($id) 
    {
        $this->post = $this->getPost($id);
        $this->out('post.tpl.php');
    }

    public function crud() 
    {
        $this->list = $this->db->query("SELECT * FROM post ORDER BY id")->all();
        $this->out('crud.tpl.php');
    }        
}