$(function() {
	$('<a href="#" class="close">x</a>').appendTo('.notification');

	$('.close').click(function() {
		$(this).parent('.notification').fadeOut(300);
		return false;
	});

});