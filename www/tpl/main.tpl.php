<html>
<head>
	<title>habramambr</title>
	<meta charset="utf-8">

	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body>
	<div class="wrap">
		<header>
			<div class="logo left">
				<h1>
					<a href="/">Habr!</a>
				</h1>
			</div>
			<div class="left navigation">
				<nav>
					<ul>
						<li>
							<a href="/">Home</a>
						</li>
						<li>
							<a href="/?crud">crud</a>
						</li>
						<li>
							<a href="/?add">Add</a>
						</li>
					</ul>
				</nav>
			</div>
			<div class="clear"></div>
		</header>

		<div class="content">
			<?php $this->out($this->tpl, true); ?>
		</div>
	

		<footer>
		</footer>
	</div>

	<script type="text/javascript" src="/assets/js/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="/assets/js/script.js"></script>
</body>
</html>