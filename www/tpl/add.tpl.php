<?php if (!empty($this->sucess)): ?>
	<div class="sucess notification"><?=@ $this->sucess ?></div>
<?php endif; ?>
	
<form method="post">
	<label for="title">Title</label>
	<input type="text" name="title" id="title" value="<?=@ $this->title ?>">
	<label for="email">Email</label>
	<input type="email" name="email" id="email" value="<?=@ $this->email ?>">
	<label for="content">Content</label>
	<textarea name="content" id="content"><?=@ $this->content ?></textarea>

	<input type="submit">
</form>

<?php if (!empty($this->error)): ?>
	<div class="error notification"><?=@ $this->error ?></div>
<?php endif; ?>