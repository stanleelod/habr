<?php

class ctrl {

    public function __construct() {
        $this->db = new db();
                
    }

    public function out($tplname,$nested=false) {
        ob_start();
        if (!$nested) {
            $this->tpl = $tplname;

            include "tpl/main.tpl.php";
            $content = ob_get_contents();
        
        } else {
            include "tpl/" . $tplname;
            $content = ob_get_contents();
        }
        ob_end_clean();
        echo $content;
    }
 
    public function cutString($string, $maxlen) {
        $len = (mb_strlen($string) > $maxlen)
            ? mb_strripos(mb_substr($string, 0, $maxlen), ' ')
            : $maxlen
        ;
        $cutStr = mb_substr($string, 0, $len);
        return (mb_strlen($string) > $maxlen)
            ? $cutStr . '...'
            : $cutStr
        ;
    }

    public function getPost($id) 
    {
        $this->post = $this->db->query("SELECT * FROM post WHERE id=?", $id)->assoc();
    }

    public function getUserId($email) 
    {
        $users = $this->db->query("SELECT id FROM users WHERE email = ?", $email)->assoc();
        
        if (!$users['id']) {
            $user_id = $this->db->query("INSERT INTO users (email) VALUES (?)", trim($_POST['email']))->getId();
        } else {
            $user_id = $users['id'];
        }
        return $user_id;
    }

    public function getUser($user_id) 
    {
        $this->user = $this->db->query("SELECT * FROM users WHERE id=?", $user_id)->assoc();
    }

    public function validEmail($email) 
    {
        return $email;
    }

    public function redirect($location=NULL) {
        header("Location: /".$location);
    }
}


//route
class app {

    public function __construct($path) {
        // http://blog.ramzes.name/?add
        $this->route = explode('/', $path);

        $this->run();
    }

    private function run() 
    {
        $url = array_shift($this->route);

        if (!preg_match('#^[a-zA-Z0-9.,-]*$#', $url))
            throw new Exception('Invalid path');

        $ctrlName = 'ctrl' . ucfirst($url);
        if (file_exists('app/' . $ctrlName.'.php')) {
            $this->runController($ctrlName);
        } else {
            array_unshift($this->route, $url);
            $this->runController('ctrlIndex');
        }
    }

    private function runController($ctrlName) {
        include "app/" . $ctrlName . ".php";

        $ctrl = new $ctrlName();

        if (empty($this->route) || empty($this->route[0])) {
            $ctrl->index();
        } else {
            if (empty($this->route))
                $method = 'index';
            else
                $method = array_shift($this->route);
            if (method_exists($ctrl, $method)) {
                if (empty($this->route))
                $ctrl->$method();
                else
                    call_user_func_array (array($ctrl,$method), $this->route);
            } else
                throw new Exception('Error 404');
        }
    }

}