<?php 
    
/**
* connect to Data Base and create sql query
*/
class db
{
    
    public function __construct() 
    {
        $conf = parse_ini_file('/config.ini');
        $this->mysqli = new mysqli($conf['mysql_host'],$conf['mysql_user'],$conf['mysql_password'], $conf['mysql_database']);
    }
    public function query($sql) 
    {
        // $db->query("SELECT * FROM aslkd WHERE id = ?",$id);
        
        $args = func_get_args();
        
        $sql = array_shift($args);
        $link = $this->mysqli;
        
        $args = array_map(function ($param) use ($link) {
            return "'".$link->escape_string($param)."'";
        },$args);
        
        $sql = str_replace(array('%','?'), array('%%','%s'), $sql);

        array_unshift($args, $sql);
        
        $sql = call_user_func_array('sprintf', $args);
        
        
        $this->last = $this->mysqli->query($sql);
        if ($this->last === false) throw new Exception('Database error: '.$this->mysqli->error);

        return $this;       
    }

    public function assoc() 
    {
        return $this->last->fetch_assoc();
    }
    
    public function all() 
    {
        $result = array();
        while ($row = $this->last->fetch_assoc()) $result[] = $row;
        return $result;
    }

    public function getId() 
    {
        $this->insert_id = $this->mysqli->insert_id;
        return $this->insert_id;
    }
}